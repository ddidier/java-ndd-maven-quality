
NDD Maven Quality
=================

## Version 0.3.0

- feat: add CI build
- feat: refactor all and rename to ndd-maven-quality
- ci: move from BitBucket to GitLab

## Version 0.2.6

- [QUALITY] Update Eclipse cleaner
- [QUALITY] Update CheckStyle configuration

## Version 0.2.5

- [DOCUMENTATION] Update site
- [DOCUMENTATION] Update NDD images
- [DOCUMENTATION] Updated Eclipse configuration guide
- [QUALITY] Reformat the POM
- [QUALITY] Update Eclipse cleaner
- [QUALITY] Update Eclipse formatter
- [QUALITY] Update Eclipse templates
- [QUALITY] Updated Eclipse dictionary
- [QUALITY] Update CheckStyle configuration to version 6.19

## Version 0.2.4

- [BUILD] Added Maven version prerequisite
- [QUALITY] Updated Checkstyle configuration to version 6.3
- [QUALITY] Updated Eclipse dictionary
- [QUALITY] Removed PMD final parameters check

## Version 0.2.3

- [DOCUMENTATION] Updated Eclipse configuration guide
- [QUALITY] Removed 'Factory$' from AbstractClassName check
- [QUALITY] Removed final parameter check
- [QUALITY] Added PMD rules set, see [NDD-BUILD-2](https://bitbucket.org/ndd-java/ndd-build/issue/2/add-pmd-report)

## Version 0.2.2

- [QUALITY] Updated Eclipse templates
- [DOCUMENTATION] Added Eclipse configuration HowTo
- [DOCUMENTATION] Added this changelog

## Version 0.2.1

- [DEPENDENCY] Updated the version of the `ndd-parent` dependency to `0.2.1`

## Version 0.2.0

- [QUALITY] Added Checkstyle import control check
- [QUALITY] Added Eclipse dictionary

## Version 0.1.2

- [QUALITY] Updated Checkstyle configuration
- [QUALITY] Updated Eclipse formatter
- [QUALITY] Updated Eclipse templates

## Version 0.1.1

- [QUALITY] Updated Checkstyle configuration
- [QUALITY] Updated Eclipse templates

## Version 0.1.0

- Initial commit
